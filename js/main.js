
// blog gallery
let body_s = $('body'),
  window_s = $(window);

//= ====== ISOTOP FILTERING JS  ========//
window_s.on('load', () => {
  let grid_container = $('.portfolio-container'),
    grid_item = $('.work');


  grid_container.imagesLoaded(() => {
    grid_container.isotope({
      itemSelector: '.work',
      layoutMode: 'masonry',
    });
  });

  $('.portfolio-filter').find('li').on('click', function (e) {
    $('.portfolio-filter li.active').removeClass('active');
    $(this).addClass('active');
    const selector = $(this).attr('data-filter');
    grid_container.isotope({
      filter: selector,
    });
    return false;
    e.preventDefault();
  });
});

// scroll to top
$('#back-top a').on('click', () => {
  $('body,html').animate({
    scrollTop: 0,
  }, 600);
  return false;
});

window.onscroll = function () {
  if (pageYOffset >= 200) {
    document.getElementById('back-top').style.visibility = 'visible';
  } else {
    document.getElementById('back-top').style.visibility = 'hidden';
  }
};

// btn next in tab content
$('.btnNext').click(() => {
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
  $('body,html').animate({
    scrollTop: 0,
  }, 600);
});

$('.btnPrevious').click(() => {
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
  $('body,html').animate({
    scrollTop: 0,
  }, 600);
});

// wow function

$(() => {
  new WOW().init();
});

$(() => {
  wow = new WOW(
    {
      boxClass: 'wow', // default
      animateClass: 'animated', // default
      offset: 0, // default
      mobile: true, // default
      live: true, // default
    },
  );
  wow.init();
});

// sidemenu function

function openNav() {
  document.getElementById('mySidenav').style.width = '280px';
  $('.overlay2').show();
}

function closeNav() {
  document.getElementById('mySidenav').style.width = '0';
  document.body.style.backgroundColor = 'white';
}

// login-menu transition 

$('.dropdown').on('show.bs.dropdown', function () {
  $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
});
// Add slideUp animation to Bootstrap dropdown when collapsing.
$('.dropdown').on('hide.bs.dropdown', function () {
  $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
});


// close sidemeu when click outside 

$(document).ready(() => {
  $('.menu-btn').click((e) => {
    e.preventDefault();
    e.stopPropagation();
    $('mySidenav').toggle();
  });


  $('.overlay2').click(() => {
    document.getElementById('mySidenav').style.width = '0';
    $('.overlay2').hide();
  });
});

const dropdown = document.getElementsByClassName('dropdown-btn');
let i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener('click', function () {
    this.classList.toggle('active');
    const dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === 'block') {
      dropdownContent.style.display = 'none';
    } else {
      dropdownContent.style.display = 'block';
    }
  });
}

// checkbox like radio

$('.input-hidden-cat').change(function () {
  $('.input-hidden-cat').prop('checked', false);
  $(this).prop('checked', true);
});

$('.input-hidden-sponsor').change(function () {
  $('.input-hidden-cat').prop('checked', false);
  $(this).prop('checked', true);
});


function standard() {
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 1);
  });

  $('.seat-disabled').click(() => false);
}
function double() {
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 2);
  });
  $('.seat-disabled').click(() => false);
}
function land() {
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 4);
  });
  $('.seat-disabled').click(() => false);
}


//  input per-category

function cat1() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  document.getElementById('category1-input1').style.pointerEvents = 'auto';
  document.getElementById('category1-input2').style.pointerEvents = 'auto';
  document.getElementById('category2-input1').style.pointerEvents = 'none';
  document.getElementById('category3-input1').style.pointerEvents = 'none';
  document.getElementById('category4-input1').style.pointerEvents = 'none';
  document.getElementById('category5-input1').style.pointerEvents = 'none';
}
function cat2() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  document.getElementById('category1-input1').style.pointerEvents = 'none';
  document.getElementById('category1-input2').style.pointerEvents = 'none';
  document.getElementById('category2-input1').style.pointerEvents = 'auto';
  document.getElementById('category3-input1').style.pointerEvents = 'none';
  document.getElementById('category4-input1').style.pointerEvents = 'none';
  document.getElementById('category5-input1').style.pointerEvents = 'none';
}
function cat3() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  document.getElementById('category1-input1').style.pointerEvents = 'none';
  document.getElementById('category1-input2').style.pointerEvents = 'none';
  document.getElementById('category2-input1').style.pointerEvents = 'none';
  document.getElementById('category3-input1').style.pointerEvents = 'auto';
  document.getElementById('category4-input1').style.pointerEvents = 'none';
  document.getElementById('category5-input1').style.pointerEvents = 'none';
}
function cat4() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  document.getElementById('category1-input1').style.pointerEvents = 'none';
  document.getElementById('category1-input2').style.pointerEvents = 'none';
  document.getElementById('category2-input1').style.pointerEvents = 'none';
  document.getElementById('category3-input1').style.pointerEvents = 'none';
  document.getElementById('category4-input1').style.pointerEvents = 'auto';
  document.getElementById('category5-input1').style.pointerEvents = 'none';
}
function cat5() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  document.getElementById('category1-input1').style.pointerEvents = 'none';
  document.getElementById('category1-input2').style.pointerEvents = 'none';
  document.getElementById('category2-input1').style.pointerEvents = 'none';
  document.getElementById('category3-input1').style.pointerEvents = 'none';
  document.getElementById('category4-input1').style.pointerEvents = 'none';
  document.getElementById('category5-input1').style.pointerEvents = 'auto';
}


// sponsor booth

function sponsormain() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 1);
  });
  $('.seat-disabled').click(() => false);

  document.getElementById('main-booth-input1').style.pointerEvents = 'auto';
  document.getElementById('gold-booth-input1').style.pointerEvents = 'none';
  document.getElementById('gold-booth-input2').style.pointerEvents = 'none';
  document.getElementById('silver-booth-input1').style.pointerEvents = 'none';
  document.getElementById('silver-booth-input2').style.pointerEvents = 'none';
  document.getElementById('bronze-booth-input1').style.pointerEvents = 'none';
}

function sponsorgold() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 1);
  });
  $('.seat-disabled').click(() => false);

  document.getElementById('main-booth-input1').style.pointerEvents = 'none';
  document.getElementById('gold-booth-input1').style.pointerEvents = 'auto';
  document.getElementById('gold-booth-input2').style.pointerEvents = 'auto';
  document.getElementById('silver-booth-input1').style.pointerEvents = 'none';
  document.getElementById('silver-booth-input2').style.pointerEvents = 'none';
  document.getElementById('bronze-booth-input1').style.pointerEvents = 'none';
}

function sponsorsilver() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 1);
  });
  $('.seat-disabled').click(() => false);

  document.getElementById('main-booth-input1').style.pointerEvents = 'none';
  document.getElementById('gold-booth-input1').style.pointerEvents = 'none';
  document.getElementById('gold-booth-input2').style.pointerEvents = 'none';
  document.getElementById('silver-booth-input1').style.pointerEvents = 'auto';
  document.getElementById('silver-booth-input2').style.pointerEvents = 'auto';
  document.getElementById('bronze-booth-input1').style.pointerEvents = 'none';
}

function sponsorbronze() {
  $('input[type=checkbox]').each(function () {
    $(this).prop('checked', false);
  });
  $('.input-booth').click(function () {
    $(this).next().next().prop('disabled', !this.checked);
    $('.input-booth').not(':checked').prop('disabled', $('.input-booth:checked').length == 1);
  });
  $('.seat-disabled').click(() => false);

  document.getElementById('main-booth-input1').style.pointerEvents = 'none';
  document.getElementById('gold-booth-input1').style.pointerEvents = 'none';
  document.getElementById('gold-booth-input2').style.pointerEvents = 'none';
  document.getElementById('silver-booth-input1').style.pointerEvents = 'none';
  document.getElementById('silver-booth-input2').style.pointerEvents = 'none';
  document.getElementById('bronze-booth-input1').style.pointerEvents = 'auto';
}
